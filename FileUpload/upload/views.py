from django.shortcuts import render
import pandas as pd
from upload.forms import ProfileForm
from upload.models import Profile
from django.http import HttpResponse
from . import metaData
def perform(request):
    if 'info' in request.GET:
        info = request.GET['info']
        #return render(request,'show.html',locals())
        details = metaData.sendDetails('C:/Users/hp/PycharmProjects/FileUpload/upload/files/%s' % info)
        #df = pd.read_csv('C:/Users/hp/PycharmProjects/FileUpload/upload/files/%s' % info)
        return render(request,'show.html',context={'details':details})
        #return HttpResponse(df.to_html())
def SaveProfile(request):
    saved = False

    if request.method == "POST":
        # Get the posted form
        MyProfileForm = ProfileForm(request.POST, request.FILES)

        if MyProfileForm.is_valid():
            profile = Profile()
            profile.name = MyProfileForm.cleaned_data["name"]
            profile.file = MyProfileForm.cleaned_data["file"]
            profile.save()
            saved = True
    else:
        MyProfileForm = ProfileForm()

    return render(request, 'saved.html', locals())
