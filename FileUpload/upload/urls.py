from django.conf.urls import url
from django.views.generic import TemplateView

from upload import views

urlpatterns = [url(r'^profile/',TemplateView.as_view(
      template_name = 'profile.html')), url(r'^saved/', views.SaveProfile, name = 'saved'),
               url(r'^compute/',views.perform,name='compute'),url(r'^read/',TemplateView.as_view(
            template_name='Read.html'))]