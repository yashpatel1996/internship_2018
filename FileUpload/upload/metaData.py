import pandas as pd
def sendDetails(info):
    df = pd.read_csv(info,na_values='?')

    details={}
    details['columnName'] = list(df)
    details['dtypes'] = list(df.dtypes)
    details['NofNaN'] = list(df.isnull().sum())
    details['describe'] = df.describe().to_html()

    #details['data'] = {}
    null_col=[]
    c=0
    for i in details['NofNaN']:

        if i > 0:
            null_col.append(details['columnName'][c])
        c += 1
    print(null_col)
    details['null_col']=null_col
    df = df.head(8)
    df = df.T
    details['row_count'] = list(df)

    details['data'] = []
    for x in list(df):
        details['data'].append(list(df[x]))
    #print(details['data']['0'])
    #print(details['data'][0])
    #print(details)

    return details