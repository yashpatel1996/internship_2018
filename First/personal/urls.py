from django.conf.urls import url
from . import views  #importing veiws from the current package
urlpatterns = [
    #url(r'^$' views.index, name='index'), #url mapping for local application FirstA.
    url(r'^$', views.index, name='index'),
    url(r'^contact/', views.contact, name='contact'),
]