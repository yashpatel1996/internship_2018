from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$',views.readFile,name="Read File"),
    url(r'^DataEntry/',views.StageTableEntry,name="EntryIntoDatabase")
]