from django.apps import AppConfig


class MappingappConfig(AppConfig):
    name = 'MappingApp'
