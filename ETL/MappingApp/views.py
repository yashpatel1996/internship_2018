from django.shortcuts import render
from django.http import HttpResponse
import pandas as pd
import sqlite3
import io
import subprocess
import sys
import pymongo
from pymongo import MongoClient
from ETL.settings import AWS_ACCESS_KEY,AWS_SECRET_KEY,REGION_NAME,S3_BUCKET
import boto3

df = pd.DataFrame()
def readFile(request):
    global df
    session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY,aws_secret_access_key=AWS_SECRET_KEY)
    s3 = session.resource('s3')
    object = s3.Object('yash-awsrdss3', request.session["filename"])
    df = pd.read_excel(io.BytesIO(object.get()['Body'].read()),na_values='?')
    l = list(df)
    return render(request,"Mapping.html",{"columnName": l})
def StageTableEntry(request):
    l = list(df)
    columnName = []
    columnMap = {}
    UniqueMap = {"u":1,"nu":0}
    RNRMap = {"r":1,"nr":0}
    dateformat = "ymd"
    for x in l:
        s = "RNR" + str(x)
        t = "U" + str(x)
        columnName.append(request.POST[x])
        if request.POST[x] == "date" or request.POST[x] == "datetime":
            columnMap[x] = [request.POST[x],RNRMap[request.POST[s]],UniqueMap[request.POST[t]],dateformat]
        elif request.POST[x] == 'age':
            columnMap[x] = [request.POST[x], RNRMap[request.POST[s]],UniqueMap[request.POST[t]], 'val']
        else:
            columnMap[x] = [request.POST[x], RNRMap[request.POST[s]],UniqueMap[request.POST[t]]]
    #df.columns = columnName
    username = request.COOKIES["username"]
    print(username)
    db_name = str(username) + ".db"
    filename = request.session["filename"]
    tablename = filename + "stage"
    conn = sqlite3.connect(db_name)
    dbtype = 'sql'

    try:
        df.to_sql(tablename,conn)
        conn.close()
        client = MongoClient()
        db = client.ETL
        jobs_left = db.jobs_left
        jobs_left.insert_one({
            'username': username,
            'filename':filename,
            'tablename':tablename,
            'dbname':db_name,
            'dbtype':dbtype,
            'columnMap':columnMap
        })
    except Exception as e:
        print(e)
    p = subprocess.run([sys.executable, 'F:/Satyak/Internship2018/ETL/MappingApp/clean_data_updated.py'],stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    print(p.stdout.decode('ascii'))

    return HttpResponse("Success")
# Create your views here.
