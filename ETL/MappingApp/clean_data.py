import pandas as pd
import numpy as np
import re # regular expression
import pymongo
from pymongo import MongoClient
import sqlite3
import datetime  #not used yet
import dateutil  #not used yet
import timeit
# import email_validator #custom email address format validator
import time
import math
import gender_guesser.detector as gender
import phonenumbers

gen_det = gender.Detector(case_sensitive=False)


# Handle only dates
# @dates is in pandas series format format
# @date_format in string format
def handle_date(dates, date_format='none'):
    if date_format == 'ymd':
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, yearfirst=True, errors='coerce')
    elif date_format == 'ydm':
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, yearfirst=True, dayfirst=True, errors='coerce')
    elif date_format == 'dmy':
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, dayfirst=True, errors='coerce')
    elif date_format == 'mdy':
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, errors='coerce')
    else:
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, errors='coerce')
    return dates_timestamp.apply(lambda x: x.date())
    # it returns Series of type Object which has all dates in string format


# handle DateTime format
# @datetimes is in Series string format
# returns Series of type Object which has all datetimes in string format
def handle_datetime(datetimes, date_format='none'):
    if date_format == 'ymd':
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, yearfirst=True, errors='coerce', utc=True)
    elif date_format == 'ydm':
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, yearfirst=True, dayfirst=True, errors='coerce', utc=True)
    elif date_format == 'dmy':
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, dayfirst=True, errors='coerce', utc=True)
    elif date_format == 'mdy':
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, errors='coerce', utc=True)
    else:
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, errors='coerce', utc=True)
    return timestamp
    # it returns Series of type Object which has all datetimes in string format


# used by handle_time() for getting time out of timestamp
def convert_to_time(timestamp):
    try:
        return timestamp.time()
    except:
        return pd.NaT


# Handles only Time formats
# parameters
# @times : in pandas Series format
# returns pandas Series of time in String format
def handle_time(times):
    times = ' 1677-09-22 ' + times
    return pd.to_datetime(times, infer_datetime_format=False, errors='coerce').apply(lambda x: convert_to_time(x))


# Regular expression for valid email id format
WSP = r'[\s]'                                        # see 2.2.2. Structured Header Field Bodies
CRLF = r'(?:\r\n)'                                   # see 2.2.3. Long Header Fields
NO_WS_CTL = r'\x01-\x08\x0b\x0c\x0f-\x1f\x7f'        # see 3.2.1. Primitive Tokens
QUOTED_PAIR = r'(?:\\.)'                             # see 3.2.2. Quoted characters
FWS = r'(?:(?:' + WSP + r'*' + CRLF + r')?' + \
      WSP + r'+)'                                    # see 3.2.3. Folding white space and comments
CTEXT = r'[' + NO_WS_CTL + \
        r'\x21-\x27\x2a-\x5b\x5d-\x7e]'              # see 3.2.3
CCONTENT = r'(?:' + CTEXT + r'|' + \
           QUOTED_PAIR + r')'                        # see 3.2.3 (NB: The RFC includes COMMENT here
# as well, but that would be circular.)
COMMENT = r'\((?:' + FWS + r'?' + CCONTENT + \
          r')*' + FWS + r'?\)'                       # see 3.2.3
CFWS = r'(?:' + FWS + r'?' + COMMENT + ')*(?:' + \
       FWS + '?' + COMMENT + '|' + FWS + ')'         # see 3.2.3
ATEXT = r'[\w!#$%&\'\*\+\-/=\?\^`\{\|\}~]'           # see 3.2.4. Atom
ATOM = CFWS + r'?' + ATEXT + r'+' + CFWS + r'?'      # see 3.2.4
DOT_ATOM_TEXT = ATEXT + r'+(?:\.' + ATEXT + r'+)*'   # see 3.2.4
DOT_ATOM = CFWS + r'?' + DOT_ATOM_TEXT + CFWS + r'?'  # see 3.2.4
QTEXT = r'[' + NO_WS_CTL + \
        r'\x21\x23-\x5b\x5d-\x7e]'                   # see 3.2.5. Quoted strings
QCONTENT = r'(?:' + QTEXT + r'|' + \
           QUOTED_PAIR + r')'                        # see 3.2.5
QUOTED_STRING = CFWS + r'?' + r'"(?:' + FWS + \
                r'?' + QCONTENT + r')*' + FWS + \
                r'?' + r'"' + CFWS + r'?'
LOCAL_PART = r'(?:' + DOT_ATOM + r'|' + \
             QUOTED_STRING + r')'                    # see 3.4.1. Addr-spec specification
DTEXT = r'[' + NO_WS_CTL + r'\x21-\x5a\x5e-\x7e]'    # see 3.4.1
DCONTENT = r'(?:' + DTEXT + r'|' + \
           QUOTED_PAIR + r')'                        # see 3.4.1
DOMAIN_LITERAL = CFWS + r'?' + r'\[' + \
                 r'(?:' + FWS + r'?' + DCONTENT + \
                 r')*' + FWS + r'?\]' + CFWS + r'?'  # see 3.4.1
DOMAIN = r'(?:' + DOT_ATOM + r'|' + \
         DOMAIN_LITERAL + r')'                       # see 3.4.1
ADDR_SPEC = LOCAL_PART + r'@' + DOMAIN               # see 3.4.1

# A valid address will match exactly the 3.4.1 addr-spec.
VALID_ADDRESS_REGEXP = '^' + ADDR_SPEC + '$'


# For email id format validation
# old name  :  email_validator_pandas
# @col is pandas Series object
# returns Series of true and false
def email_format_validation(col):
    return col.str.match(VALID_ADDRESS_REGEXP)


# converts gender column to proper format, ex : MALE, FEMALE, OTHER
# It is called first
# it will only convert your column in proper format of gender but if the record is not gender then it will return
# that record as it is. now after this function we should call validate_gender function which will give indexes of
# error records
def gender_proper_format(col):
    formats = ["f", "male", "m", "female", "other", "o", "others", "males", "females", "fem", np.NaN]
    col_df = pd.DataFrame(col, columns=["Gender"])
    col_df['Gender'] = col_df['Gender'].str.strip()
    col_df["Gender"][(col_df["Gender"].str.lower().isin(formats)) & ((col_df["Gender"].str.lower().str[0] == 'm'))] = "MALE"
    col_df["Gender"][(col_df["Gender"].str.lower().isin(formats)) & ((col_df["Gender"].str.lower().str[0] == 'f'))] = "FEMALE"
    col_df["Gender"][(col_df["Gender"].str.lower().isin(formats)) & ((col_df["Gender"].str.lower().str[0] == 'o'))] = "OTHER"
    # col_df["Gender"][~col_df["Gender"].str.lower().isin(formats)] = "error"
    return col_df['Gender']


# Validate genders
def gender_validation(col):
    formats = ["MALE", "FEMALE", "OTHER"]
    ans = []
    gender_se = col
    for gen in gender_se:
        if gen in ['nan', np.nan]:
            ans.append(1)
        else:
            if gen in formats:
                ans.append(0)
            else:
                ans.append(1)
    return {'out_ind': np.array(ans), 'no_of_errors': ans.count(1)}


# find gender from first name
# pass all records which are NaN values or not validated values
def gender_from_name(names, genders):
    ans = []
    for index, name in names.iteritems():
        if name in [np.nan, 'nan', 'nat']:
            ans.append(1)
            # here we don't have person name for this record available so we couldn't predict gender for this record
        else:
            pred_gen = gen_det.get_gender(name)
            if pred_gen in ['male', 'mostly_male', 'andy']:
                genders[index] = "MALE"
                ans.append(0)
            elif pred_gen in ['female', 'mostly_female']:
                genders[index] = "FEMALE"
                ans.append(0)
                # True means we successfully predicted gender from name
            elif pred_gen in ['unknown']:
                ans.append(1)
                # False means we couldn't predict gender from name
    return {'out_ind': np.array(ans), 'no_of_errors': ans.count(1), 'genders': genders}
    # here we are also passing genders Series which has same size as it was before when it passed in this function.


# @ col_arr is in list or numpy array or pandas Series format
# phone number format validation
def phone_validation(col_arr):
    final_phn = []
    validation_status = []
    for phn_number in col_arr:
        phn_number = str(phn_number)
        if "+" in phn_number:
            try:
                z = phonenumbers.parse(phn_number)
                if phonenumbers.is_valid_number(z):
                    final_phn.append("+" + str(z.country_code) + "-" + str(z.national_number))
                    validation_status.append(0)
                    # print(final_phn[-1])
                    # print("Country Code: "+str(z.country_code)+" National Number: "+str(z.national_number))
                else:
                    final_phn.append(phn_number)
                    validation_status.append(1)
                    # print("invalid Number 1")
            except Exception as e:
                final_phn.append(phn_number)
                validation_status.append(1)
                # print(e+"invalid country code")
        elif phn_number in [np.nan, 'nan']:
            validation_status.append(2)
            final_phn.append(np.nan)
        else:
            phn_number = '+' + phn_number
            try:
                z = phonenumbers.parse(phn_number)
                if phonenumbers.is_valid_number(z):
                    final_phn.append("+" + str(z.country_code) + "-" + str(z.national_number))
                    validation_status.append(0)
                else:
                    final_phn.append(phn_number[1:])
                    validation_status.append(1)
            except Exception as e:
                final_phn.append(phn_number[1:])
                validation_status.append(1)
    return {"final_phn": pd.Series(final_phn), "out_ind": np.array(validation_status), "no_of_errors": validation_status.count(1)}


def phone_countrycode_addition(phn_number, country_name):
    df_country_code = pd.read_csv("F:/countrycode.csv", usecols=["CountryName", "ISO2"])
    df_country_code["CountryName"] = df_country_code["CountryName"].str.lower()
    df_country_code["CountryName"] = df_country_code["CountryName"].str.replace(" ", "")
    df_country_code = df_country_code.set_index("CountryName")
    status = []
    for ind, phn in phn_number.iteritems():
        if phn[0] is '+':
            phn = phn[1:]
        try:
            z = phonenumbers.parse(phn, df_country_code.loc[(country_name[ind].lower()).replace(" ", ""), "ISO2"])
            if phonenumbers.is_valid_number(z):
                phn_number[ind] = ("+" + str(z.country_code) + "-" + str(z.national_number))
                status.append(0)
            else:
                status.append(1)
        except Exception as e:
            status.append(1)
    return {"final_phn": phn_number, "out_ind": np.array(status), "no_of_errors": status.count(1)}


# converts currency to number
# @ currs  in list, numpy array or pandas Series
# returns only currency values not country symbol in pandas Series format
# error : all currencies must be in format 55.0 , 55.5 Rs etcs
def currency_to_number(currs):
    s = []
    for i in currs:
        i = str(i)
        if i not in ['nan', np.nan]:
            if "." in i:
                m = re.findall('(\d+)(\.)(\d+)', i)
            else:
                m = re.findall('\d+', i)
            if len(m) != 0:
                x = str("".join(m[0]))
                s.append(x)
            else:
                s.append(np.nan)
        else:
            s.append(np.nan)
    return pd.Series(s)


# @age is in pandas Series format
# returns pandas series of floats
# errors : not working for 22.1 (if we use only floating numbers)
def convert_age(age, structure="val"):
    values = []
    age = age.values
    if structure == "val":
        for x in age:
            if x in [np.nan, 'nan']:
                values.append(np.nan)
            else:
                if type(x) is not 'str':
                    x = str(x)
                # print(x)
                x = x.lower()
                age2 = re.findall('\\d+', x)
                if len(age2) != 0:
                    age2_int = list(map(int, age2))
                    if (("d" or "day" or "days" or "dy" or "dys") in x):
                        if len(age2_int) == 3:
                            age2_int[2] /= 365
                        elif len(age2_int) == 2:
                            age2_int[1] /= 365
                        else:
                            age2_int[0] /= 365
                    if (("m" or "mon" or "month" or "months") in x):
                        if len(age2_int) == 1:
                            age2_int[0] /= 12
                        else:
                            age2_int[1] /= 12
                    values.append(round(sum(age2_int), 2))
                else:
                    values.append(np.nan)
    else:
        if structure == "months":
            for x in age:
                if x in [np.nan, 'nan']:
                    values.append(np.nan)
                else:
                    x /= 12
                    x = round(x, 2)
                    values.append(float(x))
        if structure == "days":
            for x in age:
                if x in [np.nan, 'nan']:
                    values.append(np.nan)
                else:
                    x /= 365
                    x = round(x, 2)
                    values.append(float(x))
        if structure == "years":
            for x in age:
                if x in [np.nan, 'nan']:
                    values.append(np.nan)
                else:
                    values.append(float(x))
    return pd.Series(values)


# @ original_data in Series format
# @ new_data in Series format
# returns new_data :for making final data for a column which has original null values, cleaned values, and if error has occured then that data is stored as it is.
# returns job_info : has values 0 = ok record, 1 = error record , 2 = null record
def data_maker(original_data, new_data):
    x = (original_data.isnull()).values.astype(int)
    y = (new_data.isnull()).values.astype(int)
    job_info = x + y  # 0 = ok record, 1 = error record , 2 = null record
    error_data = original_data[(job_info == 1)]
    for ind, val in error_data.iteritems():
        new_data[ind] = val
    return new_data, job_info


def other_controller(col_name, val):
    pass


def email_controller(col_name, val):
    print("email parent process.........")
    email_se = df[col_name]
    # x = (email_se.isnull()).values.astype(int)
    email_status = email_format_validation(email_se).fillna(2).astype(int)
    # print(email_status)
    email_status = email_status.replace(1, 3)
    email_status = email_status.replace(0, 1)
    email_status = email_status.replace(3, 0)
    # print(email_status)
    job_info = email_status.values.astype(int)
    # y = (email_status).values.astype(int)
    # job_info = x + (~y)  # 0 = ok record, 1 = error record , 2 = null record
    return job_info


def date_controller(col_name, val):
    print("date parent process..........")
    date_se = df[col_name]  # get only required column data from stage file
    final_dates = handle_date(date_se, val[2])
    final_dates, job_info = data_maker(date_se, final_dates)
    df[col_name] = final_dates  # save back to stage file
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def time_controller(col_name, val):
    print("time parent process..........")
    time_se = df[col_name]
    final_times = handle_time(time_se)
    final_times, job_info = data_maker(time_se, final_times)
    df[col_name] = final_times  # save back to stage file
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def datetime_controller(col_name, val):
    print("datetime parent process..........")
    datetime_se = df[col_name]  # get only required column data from stage file
    final_datetimes = handle_datetime(datetime_se, val[2])
    final_datetimes, job_info = data_maker(datetime_se, final_datetimes)
    df[col_name] = final_datetimes  # save back to stage file
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def phone_controller(col_name, val):
    print("Phone parent process..........")
    phone_se = df[col_name]
    temp_data = phone_validation(phone_se)
    final_phns = temp_data['final_phn']
    # print(final_phns)
    out_ind = temp_data['out_ind']
    # print(out_ind)
    no_err = temp_data['no_of_errors']
    for_country_data = final_phns[(out_ind == 1)]
    # print(for_country_data)
    if no_err and ('country_name' in columns):
        print("phone sub process lvl:1 get country code......")
        cname_se = df[columns['country_name']][(out_ind == 1)]
        semi_final_data = phone_countrycode_addition(for_country_data, cname_se)
        semi_final_phns = semi_final_data['final_phn']
        # print(semi_final_phns)
        semi_final_out_ind = semi_final_data['out_ind']
        # print(semi_final_out_ind)
        semi_final_no_err = semi_final_data['no_of_errors']
        if no_err != semi_final_no_err:
            print("phone sub process lvl:1 get country code changed data")
            for ind, val in semi_final_phns[(semi_final_out_ind == 0)].iteritems():
                final_phns[ind] = val
                out_ind[ind] = 0
    df[col_name] = final_phns  # save back to stage file
    # print(final_phns)
    return out_ind  # same as job_info : 0 = ok record, 1 = error record , 2 = null record


def age_controller(col_name, val):
    print("age parent process..........")
    age_se = df[col_name]
    final_ages = convert_age(age_se, val[2])
    final_ages, job_info = data_maker(age_se, final_ages)
    df[col_name] = final_ages  # save back to stage file
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def gender_controller(col_name, val):
    print("gender parent process..........")
    gender_se = df[col_name]
    final_genders = gender_proper_format(gender_se.values)
    # print(final_genders)
    temp_data = gender_validation(final_genders)
    out_ind = temp_data['out_ind']
    no_err = temp_data['no_of_errors']
    for_name_data = final_genders[(out_ind == 1)]
    if no_err and ('first_name' in columns):
        print("gender sub process lvl:1 get gender from first name......")
        fname_se = df[columns['first_name']][out_ind == 1]
        semi_final_data = gender_from_name(fname_se, for_name_data)
        semi_final_genders = semi_final_data['genders']
        semi_final_out_ind = semi_final_data['out_ind']
        semi_final_no_err = semi_final_data['no_of_errors']
        # print(semi_final_genders)
        # print(no_err)
        # print(semi_final_no_err)
        if no_err != semi_final_no_err:
            print("gender sub process lvl:1 get gender from first name changed data")
            for ind, val in semi_final_genders[(semi_final_out_ind == 0)].iteritems():
                final_genders[ind] = val
                out_ind[ind] = 0
    df[col_name] = final_genders  # save back to stage file
    return out_ind  # same as job_info : 0 = ok record, 1 = error record , 2 = null record


def currency_controller(col_value, val):
    print("currency parent process..........")
    curr_se = df[col_value]
    final_currencies = currency_to_number(curr_se)
    final_currencies, job_info = data_maker(curr_se, final_currencies)
    df[col_name] = final_currencies  # save back to stage file
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def operations(x):
    return {'date': date_controller,
            'time': time_controller,
            'datetime': datetime_controller,
            'phone_number': phone_controller,
            'gender': gender_controller,
            'email': email_controller,
            'age': age_controller,
            'currency': currency_controller,
            'other': other_controller
            }.get(x, other_controller)

client = MongoClient()
ETL = client.ETL
jobs_left = ETL.jobs_left
jobs_completed = ETL.jobs_completed
for x in jobs_left.find():
    meta_data = dict(x)
    #print("type",meta_data)
    column_wise_error_inds = {}
    columns = {}
    dbname = meta_data["dbname"]
    tablename = meta_data["tablename"]
    conn = sqlite3.connect(dbname)
    df = pd.read_sql_query("SELECT * from '" + tablename + "'",conn)

    for col_name, val in meta_data['columnMap'].items():
        columns[val[0]] = col_name
    for col_name, val in meta_data['columnMap'].items():
        column_wise_error_inds[col_name] = operations(val[0])(col_name, val)
#df.to_excel('data/data_new.xlsx', index=False)
    df_error=pd.DataFrame()
    droped_indices=[]
    for key, val in column_wise_error_inds.items():
        print("{0} : {1}".format(key, val))
        if val is None:
            continue
        for i in range(len(val)):
            if val[i]==1 and i not in droped_indices:
                df_error = df_error.append(df.loc[i])
                df.drop([i],axis=0,inplace=True)
                droped_indices.append(i)
                print(droped_indices)
    df.to_sql(meta_data['filename']+"final",conn)
    df_error.to_sql(meta_data['filename']+"error",conn)
    conn.close()
    jobs_completed.insert_one(x)
    jobs_left.remove(x)


