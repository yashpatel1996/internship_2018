import pandas as pd
import numpy as np
import re  # regular expression
import datetime  #not used yet
import dateutil  #not used yet
import timeit
# import email_validator #custom email address format validator
import time
import math
import gender_guesser.detector as gender
import phonenumbers
import sqlite3
import pymongo
from pymongo import MongoClient
gen_det = gender.Detector(case_sensitive=False)
import logging
from log4mongo.handlers import BufferedMongoHandler

logger = logging.getLogger('DEMO LOGGER')
logger.setLevel(logging.DEBUG)
handler = BufferedMongoHandler(host='localhost',                          # All MongoHandler parameters are valid
                               buffer_size=3,                           # buffer size.
                               buffer_periodical_flush_timing=10.0,       # periodical flush every 10 seconds
                               buffer_early_flush_level=logging.CRITICAL) # early flush level
logger.addHandler(handler)

job_id=0
# Handle only dates
# @dates is in pandas series format format
# @date_format in string format
def handle_date(dates, date_format='none'):
    global job_id
    logger.debug("date handling in process",extra={'job_id':job_id})
    if date_format == 'ymd':
        logger.debug("handling date format 'YMD'",extra={'job_id':job_id})
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, yearfirst=True, errors='coerce')
    elif date_format == 'ydm':
        logger.debug("handling date format 'YDM'",extra={'job_id':job_id})
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, yearfirst=True, dayfirst=True, errors='coerce')
    elif date_format == 'dmy':
        logger.debug("handling date format 'DMY'",extra={'job_id':job_id})
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, dayfirst=True, errors='coerce')
    elif date_format == 'mdy':
        logger.debug("handling date format 'MDY'",extra={'job_id':job_id})
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, errors='coerce')
    else:
        logger.debug("handling DEFAULT date format",extra={'job_id':job_id})
        dates_timestamp = pd.to_datetime(dates, infer_datetime_format=True, errors='coerce')
    logger.debug("date handling terminates",extra={'job_id':job_id})
    return dates_timestamp.apply(lambda x: x.date())
    # it returns Series of type Object which has all dates in string format


# handle DateTime format
# @datetimes is in Series string format
# returns Series of type Object which has all datetimes in string format
def handle_datetime(datetimes, date_format='none'):
    global job_id
    logger.debug("datetime handling in process",extra={'job_id':job_id})
    if date_format == 'ymd':
        logger.debug("handling datetime format 'YMD'",extra={'job_id':job_id})
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, yearfirst=True, errors='coerce', utc=True)
    elif date_format == 'ydm':
        logger.debug("handling datetime format 'YDM'",extra={'job_id':job_id})
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, yearfirst=True, dayfirst=True, errors='coerce', utc=True)
    elif date_format == 'dmy':
        logger.debug("handling datetime format 'DMY'",extra={'job_id':job_id})
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, dayfirst=True, errors='coerce', utc=True)
    elif date_format == 'mdy':
        logger.debug("handling datetime format 'MDY'",extra={'job_id':job_id})
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, errors='coerce', utc=True)
    else:
        logger.debug("handling DEFAULT datetime format",extra={'job_id':job_id})
        timestamp = pd.to_datetime(datetimes, infer_datetime_format=True, errors='coerce', utc=True)
    logger.debug("datetime handling terminates",extra={'job_id':job_id})
    return timestamp
    # it returns Series of type Object which has all datetimes in string format


# used by handle_time() for getting time out of timestamp
def convert_to_time(timestamp):
    try:
        return timestamp.time()
    except:
        return pd.NaT


# Handles only Time formats
# parameters
# @times : in pandas Series format
# returns pandas Series of time in String format
def handle_time(times):
    global job_id
    logger.debug("Handling time formats",extra={'job_id':job_id})
    times = ' 1677-09-22 ' + times
    return pd.to_datetime(times, infer_datetime_format=False, errors='coerce').apply(lambda x: convert_to_time(x))


# Regular expression for valid email id format
WSP = r'[\s]'                                        # see 2.2.2. Structured Header Field Bodies
CRLF = r'(?:\r\n)'                                   # see 2.2.3. Long Header Fields
NO_WS_CTL = r'\x01-\x08\x0b\x0c\x0f-\x1f\x7f'        # see 3.2.1. Primitive Tokens
QUOTED_PAIR = r'(?:\\.)'                             # see 3.2.2. Quoted characters
FWS = r'(?:(?:' + WSP + r'*' + CRLF + r')?' + \
      WSP + r'+)'                                    # see 3.2.3. Folding white space and comments
CTEXT = r'[' + NO_WS_CTL + \
        r'\x21-\x27\x2a-\x5b\x5d-\x7e]'              # see 3.2.3
CCONTENT = r'(?:' + CTEXT + r'|' + \
           QUOTED_PAIR + r')'                        # see 3.2.3 (NB: The RFC includes COMMENT here
# as well, but that would be circular.)
COMMENT = r'\((?:' + FWS + r'?' + CCONTENT + \
          r')*' + FWS + r'?\)'                       # see 3.2.3
CFWS = r'(?:' + FWS + r'?' + COMMENT + ')*(?:' + \
       FWS + '?' + COMMENT + '|' + FWS + ')'         # see 3.2.3
ATEXT = r'[\w!#$%&\'\*\+\-/=\?\^`\{\|\}~]'           # see 3.2.4. Atom
ATOM = CFWS + r'?' + ATEXT + r'+' + CFWS + r'?'      # see 3.2.4
DOT_ATOM_TEXT = ATEXT + r'+(?:\.' + ATEXT + r'+)*'   # see 3.2.4
DOT_ATOM = CFWS + r'?' + DOT_ATOM_TEXT + CFWS + r'?'  # see 3.2.4
QTEXT = r'[' + NO_WS_CTL + \
        r'\x21\x23-\x5b\x5d-\x7e]'                   # see 3.2.5. Quoted strings
QCONTENT = r'(?:' + QTEXT + r'|' + \
           QUOTED_PAIR + r')'                        # see 3.2.5
QUOTED_STRING = CFWS + r'?' + r'"(?:' + FWS + \
                r'?' + QCONTENT + r')*' + FWS + \
                r'?' + r'"' + CFWS + r'?'
LOCAL_PART = r'(?:' + DOT_ATOM + r'|' + \
             QUOTED_STRING + r')'                    # see 3.4.1. Addr-spec specification
DTEXT = r'[' + NO_WS_CTL + r'\x21-\x5a\x5e-\x7e]'    # see 3.4.1
DCONTENT = r'(?:' + DTEXT + r'|' + \
           QUOTED_PAIR + r')'                        # see 3.4.1
DOMAIN_LITERAL = CFWS + r'?' + r'\[' + \
                 r'(?:' + FWS + r'?' + DCONTENT + \
                 r')*' + FWS + r'?\]' + CFWS + r'?'  # see 3.4.1
DOMAIN = r'(?:' + DOT_ATOM + r'|' + \
         DOMAIN_LITERAL + r')'                       # see 3.4.1
ADDR_SPEC = LOCAL_PART + r'@' + DOMAIN               # see 3.4.1

# A valid address will match exactly the 3.4.1 addr-spec.
VALID_ADDRESS_REGEXP = '^' + ADDR_SPEC + '$'


# For email id format validation
# old name  :  email_validator_pandas
# @col is pandas Series object
# returns Series of true and false
def email_format_validation(col):
    return col.str.match(VALID_ADDRESS_REGEXP)


# converts gender column to proper format, ex : MALE, FEMALE, OTHER
# It is called first
# it will only convert your column in proper format of gender but if the record is not gender then it will return
# that record as it is. now after this function we should call validate_gender function which will give indexes of
# error records
def gender_proper_format(col):
    global job_id
    logger.debug("gender formating process starts",extra={'job_id':job_id})
    formats = ["f", "male", "m", "female", "other", "o", "others", "males", "females", "fem", np.NaN]
    col_df = pd.DataFrame(col, columns=["Gender"])
    col_df['Gender'] = col_df['Gender'].str.strip()
    col_df["Gender"][(col_df["Gender"].str.lower().isin(formats)) & ((col_df["Gender"].str.lower().str[0] == 'm'))] = "MALE"
    col_df["Gender"][(col_df["Gender"].str.lower().isin(formats)) & ((col_df["Gender"].str.lower().str[0] == 'f'))] = "FEMALE"
    col_df["Gender"][(col_df["Gender"].str.lower().isin(formats)) & ((col_df["Gender"].str.lower().str[0] == 'o'))] = "OTHER"
    # col_df["Gender"][~col_df["Gender"].str.lower().isin(formats)] = "error"
    logger.debug("gender formating process terminates",extra={'job_id':job_id})
    return col_df['Gender']


# Validate genders
def gender_validation(col):
    formats = ["MALE", "FEMALE", "OTHER"]
    ans = []
    gender_se = col
    for gen in gender_se:
        if gen in ['nan', np.nan]:
            ans.append(1)
        else:
            if gen in formats:
                ans.append(0)
            else:
                ans.append(1)
    return {'out_ind': np.array(ans), 'no_of_errors': ans.count(1)}


# find gender from first name
# pass all records which are NaN values or not validated values
def gender_from_name(names, genders):
    ans = []
    for index, name in names.iteritems():
        if name in [np.nan, 'nan', 'nat']:
            ans.append(1)
            # here we don't have person name for this record available so we couldn't predict gender for this record
        else:
            pred_gen = gen_det.get_gender(name)
            if pred_gen in ['male', 'mostly_male', 'andy']:
                genders[index] = "MALE"
                ans.append(0)
            elif pred_gen in ['female', 'mostly_female']:
                genders[index] = "FEMALE"
                ans.append(0)
                # True means we successfully predicted gender from name
            elif pred_gen in ['unknown']:
                ans.append(1)
                # False means we couldn't predict gender from name
    return {'out_ind': np.array(ans), 'no_of_errors': ans.count(1), 'genders': genders}
    # here we are also passing genders Series which has same size as it was before when it passed in this function.


# @ col_arr is in list or numpy array or pandas Series format
# phone number format validation
def phone_validation(col_arr):
    global job_id
    logger.debug("phone validation process without country name, starts",extra={'job_id':job_id})
    final_phn = []
    validation_status = []
    for phn_number in col_arr:
        phn_number = str(phn_number)
        if "+" in phn_number:
            try:
                z = phonenumbers.parse(phn_number)
                if phonenumbers.is_valid_number(z):
                    final_phn.append("+" + str(z.country_code) + "-" + str(z.national_number))
                    validation_status.append(0)
                    # logger.debug(final_phn[-1])
                    # logger.debug("Country Code: "+str(z.country_code)+" National Number: "+str(z.national_number))
                else:

                    final_phn.append(phn_number)
                    validation_status.append(1)
                    # logger.debug("invalid Number 1")
            except Exception as e:
                final_phn.append(phn_number)
                validation_status.append(1)
                # logger.debug(e+"invalid country code")
        elif phn_number in [np.nan, 'nan', 'None']:
            validation_status.append(2)
            final_phn.append(np.nan)
        else:
            phn_number = '+' + phn_number
            try:
                z = phonenumbers.parse(phn_number)
                if phonenumbers.is_valid_number(z):
                    final_phn.append("+" + str(z.country_code) + "-" + str(z.national_number))
                    validation_status.append(0)
                else:
                    final_phn.append(phn_number[1:])
                    validation_status.append(1)
            except Exception as e:
                final_phn.append(phn_number[1:])
                validation_status.append(1)
    logger.debug("phone validation process without country name, terminates",extra={'job_id':job_id})
    return {"final_phn": pd.Series(final_phn), "out_ind": np.array(validation_status), "no_of_errors": validation_status.count(1)}


def phone_countrycode_addition(phn_number, country_name):

    df_country_code = pd.read_csv("F:/Satyak/Datafiles/countrycode.csv", usecols=["CountryName", "ISO2"])
    df_country_code["CountryName"] = df_country_code["CountryName"].str.lower()
    df_country_code["CountryName"] = df_country_code["CountryName"].str.replace(" ", "")
    df_country_code = df_country_code.set_index("CountryName")
    status = []
    for ind, phn in phn_number.iteritems():
        if phn[0] is '+':
            phn = phn[1:]
        try:
            z = phonenumbers.parse(phn, df_country_code.loc[(country_name[ind].lower()).replace(" ", ""), "ISO2"])
            if phonenumbers.is_valid_number(z):
                phn_number[ind] = ("+" + str(z.country_code) + "-" + str(z.national_number))
                status.append(0)
            else:
                status.append(1)
        except Exception as e:
            status.append(1)
    return {"final_phn": phn_number, "out_ind": np.array(status), "no_of_errors": status.count(1)}


# converts currency to number
# @ currs  in list, numpy array or pandas Series
# returns only currency values not country symbol in pandas Series format
# error : all currencies must be in format 55.0 , 55.5 Rs etcs
def currency_to_number(currs):
    global job_id
    logger.debug("fetching digits from currency",extra={'job_id':job_id})
    s = []
    for i in currs:
        if i not in ['nan', np.nan]:
            i = str(i)
            if "." in i:
                m = re.findall('(\d+)(\.)(\d+)', i)
            else:
                m = re.findall('\d+', i)
            if len(m) != 0:
                x = str("".join(m[0]))
                s.append(x)
            else:
                s.append(np.nan)
        else:
            s.append(np.nan)
    return pd.Series(s)


# @age is in pandas Series format
# returns pandas series of floats
# errors : not working for 22.1 (if we use only floating numbers)
def convert_age(age, structure="val"):
    global job_id
    values = []
    logger.debug("formating age process starts",extra={'job_id':job_id})
    age = age.values
    if structure == "val":
        for x in age:
            if x in [np.nan, 'nan']:
                values.append(np.nan)
            else:
                if type(x) is not 'str':
                    x = str(x)
                # logger.debug(x)
                x = x.lower()
                age2 = re.findall('\\d+', x)
                if len(age2) != 0:
                    age2_int = list(map(int, age2))
                    if (("d" or "day" or "days" or "dy" or "dys") in x):
                        if len(age2_int) == 3:
                            age2_int[2] /= 365
                        elif len(age2_int) == 2:
                            age2_int[1] /= 365
                        else:
                            age2_int[0] /= 365
                    if (("m" or "mon" or "month" or "months") in x):
                        if len(age2_int) == 1:
                            age2_int[0] /= 12
                        else:
                            age2_int[1] /= 12
                    values.append(round(sum(age2_int), 2))
                else:
                    values.append(np.nan)
    else:
        if structure == "months":
            for x in age:
                if x in [np.nan, 'nan']:
                    values.append(np.nan)
                else:
                    x /= 12
                    x = round(x, 2)
                    values.append(float(x))
        if structure == "days":
            for x in age:
                if x in [np.nan, 'nan']:
                    values.append(np.nan)
                else:
                    x /= 365
                    x = round(x, 2)
                    values.append(float(x))
        if structure == "years":
            for x in age:
                if x in [np.nan, 'nan']:
                    values.append(np.nan)
                else:
                    values.append(float(x))
    logger.debug("age formating process terminates",extra={'job_id':job_id})
    return pd.Series(values)


def fill_values_by_unique(final_data, job_info):
    for ue in unique_entity_values:
        ext_data = final_data[unique_columns_data[unique_columns_name[0]] == ue][job_info[unique_columns_data[unique_columns_name[0]] == ue] == 0]
        if not ext_data.empty:
            correct_data = ext_data.iloc[-1]
            final_data.loc[(job_info[unique_columns_data[unique_columns_name[0]] == ue] != 0).index] = correct_data
            job_info.loc[(job_info[unique_columns_data[unique_columns_name[0]] == ue] != 0).index] = 0
    return final_data, job_info


# @ original_data in Series format
# @ new_data in Series format
# returns new_data :for making final data for a column which has original null values, cleaned values, and if error has occured then that data is stored as it is.
# returns job_info : has values 0 = ok record, 1 = error record , 2 = null record
def data_maker(original_data, new_data):
    x = (original_data.isnull()).values.astype(int)
    y = (new_data.isnull()).values.astype(int)
    job_info = x + y  # 0 = ok record, 1 = error record , 2 = null record
    error_data = original_data[(job_info == 1)]
    for ind, val in error_data.iteritems():
        new_data[ind] = val
    return new_data, job_info


def other_controller(col_name, value):
    global job_id
    data_se = df[col_name]
    job_info = data_se.isnull().astype(int).replace(1, 2)
    if unique_columns_name and (unique_columns_name[0] is not col_name) and unique_entity_values:
        logger.debug(col_name + " other filling errors and NaNs using unique column",extra={'job_id':job_id})
        data_se, job_info = fill_values_by_unique(data_se, job_info)
        df[col_name] = data_se  # save back to stage file
    if value[2] == 1:
        return job_info, data_se
    return job_info


def email_controller(col_name, value):
    global job_id
    logger.debug("email parent process starts",extra={'job_id':job_id})
    email_se = df[col_name]  # get only required column data from stage file
    # x = (email_se.isnull()).values.astype(int)
    email_status = email_format_validation(email_se).fillna(2).astype(int)
    # logger.debug(email_status)
    email_status = email_status.replace(1, 3)
    email_status = email_status.replace(0, 1)
    email_status = email_status.replace(3, 0)
    # logger.debug(email_status)
    job_info = email_status.values.astype(int)
    job_info = pd.Series(job_info)
    # y = (email_status).values.astype(int)
    # job_info = x + (~y)  # 0 = ok record, 1 = error record , 2 = null record
    if unique_columns_name and (unique_columns_name[0] is not col_name) and unique_entity_values:
        logger.debug("filling errors and NaNs values using unique column constraint in email",extra={'job_id':job_id})
        email_se, job_info = fill_values_by_unique(email_se, job_info)
    df[col_name] = email_se
    if value[2] == 1:
        return job_info, email_se
    return job_info


def date_controller(col_name, value):
    global job_id
    logger.debug("date parent process starts",extra={'job_id':job_id})
    date_se = df[col_name]  # get only required column data from stage file
    final_dates = handle_date(date_se, val[3])
    final_dates, job_info = data_maker(date_se, final_dates)
    job_info = pd.Series(job_info)
    if unique_columns_name and (unique_columns_name[0] is not col_name) and unique_entity_values:
        logger.debug("filling errors and NaNs values using unique column constraint in date",extra={'job_id':job_id})
        final_dates, job_info = fill_values_by_unique(final_dates, job_info)
    df[col_name] = final_dates  # save back to stage file
    if value[2] == 1:
        return job_info, final_dates
    logger.debug("date parent process terminates",extra={'job_id':job_id})
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def time_controller(col_name, value):
    global job_id
    logger.debug("time parent process starts",extra={'job_id':job_id})
    time_se = df[col_name]
    final_times = handle_time(time_se)
    logger.debug("Time formatting terminates",extra={'job_id':job_id})
    final_times, job_info = data_maker(time_se, final_times)
    job_info = pd.Series(job_info)
    if unique_columns_name and (unique_columns_name[0] is not col_name) and unique_entity_values:
        logger.debug("filling errors and NaNs values using unique column constraint in time",extra={'job_id':job_id})
        final_times, job_info = fill_values_by_unique(final_times, job_info)
    df[col_name] = final_times  # save back to stage file
    if value[2] == 1:
        return job_info, final_times
    logger.debug("time parent process terminates",extra={'job_id':job_id})
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def datetime_controller(col_name, value):
    global job_id
    logger.debug("datetime parent process starts",extra={'job_id':job_id})
    datetime_se = df[col_name]  # get only required column data from stage file
    final_datetimes = handle_datetime(datetime_se, val[3])
    final_datetimes, job_info = data_maker(datetime_se, final_datetimes)
    job_info = pd.Series(job_info)
    if unique_columns_name and (unique_columns_name[0] is not col_name) and unique_entity_values:
        logger.debug("filling errors and NaNs values using unique column constraint in datetime",extra={'job_id':job_id})
        final_datetimes, job_info = fill_values_by_unique(final_datetimes, job_info)
    df[col_name] = final_datetimes  # save back to stage file
    if value[2] == 1:
        return job_info, final_datetimes
    logger.debug("datetime parent process terminates",extra={'job_id':job_id})
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def phone_controller(col_name, value):
    global job_id
    logger.debug("Phone parent process starts",extra={'job_id':job_id})
    phone_se = df[col_name]
    temp_data = phone_validation(phone_se)
    final_phns = temp_data['final_phn']
    # logger.debug(final_phns)
    out_ind = temp_data['out_ind']
    # logger.debug(out_ind)
    no_err = temp_data['no_of_errors']
    for_country_data = final_phns[(out_ind == 1)]
    # logger.debug(for_country_data)
    if no_err and ('country_name' in columns):
        logger.debug("phone sub process:get country code, starts",extra={'job_id':job_id})
        cname_se = df[columns['country_name']][(out_ind == 1)]
        semi_final_data = phone_countrycode_addition(for_country_data, cname_se)
        semi_final_phns = semi_final_data['final_phn']
        # logger.debug(semi_final_phns)
        semi_final_out_ind = semi_final_data['out_ind']
        # logger.debug(semi_final_out_ind)
        semi_final_no_err = semi_final_data['no_of_errors']
        if no_err != semi_final_no_err:
            logger.debug("phone sub process:get changed data based on country code",extra={'job_id':job_id})
            for ind, val in semi_final_phns[(semi_final_out_ind == 0)].iteritems():
                final_phns[ind] = val
                out_ind[ind] = 0
        else:
            logger.debug("No data affected by country code",extra={'job_id':job_id})
    else:
        logger.debug("No country_name column found",extra={'job_id':job_id})
    out_ind = pd.Series(out_ind)
    if unique_columns_name and (unique_columns_name[0] is not col_name) and unique_entity_values:
        logger.debug("filling errors and NaNs values using unique column constraint in phone numbers",extra={'job_id':job_id})
        final_phns, out_ind = fill_values_by_unique(final_phns, out_ind)
    df[col_name] = final_phns  # save back to stage file
    # logger.debug(final_phns)
    if value[2] == 1:
        return out_ind, final_phns
    logger.debug("Phone parent process terminates",extra={'job_id':job_id})
    return out_ind  # same as job_info : 0 = ok record, 1 = error record , 2 = null record


def age_controller(col_name, value):
    global job_id
    logger.debug("age parent process starts",extra={'job_id':job_id})
    age_se = df[col_name]
    final_ages = convert_age(age_se, val[3])
    final_ages, job_info = data_maker(age_se, final_ages)
    job_info = pd.Series(job_info)
    if unique_columns_name and (unique_columns_name[0] is not col_name) and unique_entity_values:
        logger.debug("filling errors and NaNs values using unique column constraint in age",extra={'job_id':job_id})
        final_ages, job_info = fill_values_by_unique(final_ages, job_info)
    df[col_name] = final_ages  # save back to stage file
    if value[2] == 1:
        return job_info, final_ages
    logger.debug("age parent process terminates",extra={'job_id':job_id})
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def gender_controller(col_name, value):
    global job_id
    logger.debug("gender parent process starts",extra={'job_id':job_id})
    gender_se = df[col_name]
    final_genders = gender_proper_format(gender_se.values)
    # logger.debug(final_genders)
    temp_data = gender_validation(final_genders)
    out_ind = temp_data['out_ind']
    no_err = temp_data['no_of_errors']
    for_name_data = final_genders[(out_ind == 1)]
    if no_err and ('first_name' in columns):
        logger.debug("gender sub process:get gender from first name",extra={'job_id':job_id})
        fname_se = df[columns['first_name']][out_ind == 1]
        semi_final_data = gender_from_name(fname_se, for_name_data)
        semi_final_genders = semi_final_data['genders']
        semi_final_out_ind = semi_final_data['out_ind']
        semi_final_no_err = semi_final_data['no_of_errors']
        # logger.debug(semi_final_genders)
        # logger.debug(no_err)
        # logger.debug(semi_final_no_err)
        if no_err != semi_final_no_err:
            logger.debug("gender sub process:get changed gender date based on first name",extra={'job_id':job_id})
            for ind, val in semi_final_genders[(semi_final_out_ind == 0)].iteritems():
                final_genders[ind] = val
                out_ind[ind] = 0
    out_ind = pd.Series(out_ind)
    if unique_columns_name and (unique_columns_name[0] is not col_name) and unique_entity_values:
        logger.debug("filling errors and NaNs values using unique column constraint in gender",extra={'job_id':job_id})
        final_genders, out_ind = fill_values_by_unique(final_genders, out_ind)
    df[col_name] = final_genders  # save back to stage file
    if value[2] == 1:
        return out_ind, final_genders
    logger.debug("gender parent process terminates",extra={'job_id':job_id})
    return out_ind  # same as job_info : 0 = ok record, 1 = error record , 2 = null record


def currency_controller(col_value, value):
    global job_id
    logger.debug("currency parent process starts",extra={'job_id':job_id})
    curr_se = df[col_value]
    final_currencies = currency_to_number(curr_se)
    final_currencies, job_info = data_maker(curr_se, final_currencies)
    job_info = pd.Series(job_info)
    if unique_columns_name and (unique_columns_name[0] is not col_name) and unique_entity_values:
        logger.debug("filling errors and NaNs values using unique column constraint in currency",extra={'job_id':job_id})
        final_currencies, job_info = fill_values_by_unique(final_currencies, job_info)
    df[col_name] = final_currencies  # save back to stage file
    if value[2] == 1:
        return job_info, final_currencies
    logger.debug("currency parent process terminates",extra={'job_id':job_id})
    return job_info  # 0 = ok record, 1 = error record , 2 = null record


def operations(x):
    global job_id
    logger.debug("Cleaning process starts",extra={'job_id':job_id})
    return {'date': date_controller,
            'time': time_controller,
            'datetime': datetime_controller,
            'phone_number': phone_controller,
            'gender': gender_controller,
            'email': email_controller,
            'age': age_controller,
            'currency': currency_controller,
            'other': other_controller
            }.get(x, other_controller)


'''df = pd.read_excel('data/data1.xlsx', na_values='?')
con = conn = sqlite3.connect("temp.db")
df.to_sql('original_data', con, if_exists='replace')
'''
# GLOBAL varibales
#meta_data = {'Date': ['date', 0, 0, 'ymd'], 'Time': ['time', 0, 0], 'Phone_Number': ['phone_number', 0, 0], 'Gender': ['gender', 0, 0], 'First_Name': ['first_name', 0, 0], 'Email': ['email', 1, 1], 'Age': ['age', 0, 0, 'val'], 'Country name': ['country_name', 0, 0], 'Random': ['other', 0, 0]}
column_wise_error_inds = {}  # dictionary of error index for each columns which is in pandas Series format
columns = {}  # column name that we understand to acctual column name mapping
unique_columns = {}  # exraction of unique columns from meta_data, it in same format as meta_data
unique_columns_name = []  # list of unique columns name
unique_columns_data = {}  # unique columns data which has data in pandas Series format
usercolumns=[]
completed_process=[]
'''
# for getting values for above global variables
for col_name, val in meta_data.items():
    columns[val[0]] = col_name
    if val[2] == 1:
        unique_columns[col_name] = val
        unique_columns_name.append(col_name)

# for completing cleaning process for unique columns first
for col_name, val in unique_columns.items():
    column_wise_error_inds[col_name], unique_columns_data[col_name] = operations(val[0])(col_name, val)

# # GLOBAL variable
# For now we are ASSUMING that we have only one unique column that's why we are using unique_columns_name[0]
# @unique_entity_values is in list format
# only correct unique values are selectedn no nulls and errors
# using this we will fill nan and errors of other columns data in cleaning process
unique_entity_values = unique_columns_data[unique_columns_name[0]][column_wise_error_inds[unique_columns_name[0]] == 0].unique().tolist()

# for completing cleaning process of other columns which are not unique and if we have a unique column preset
# in data table then we will use unique_entity_values
for col_name, val in meta_data.items():
    if col_name not in unique_columns:
        column_wise_error_inds[col_name] = operations(val[0])(col_name, val)

# df_temp = pd.DataFrame([])
# for e in unique_entity_values:
    # df_temp = df_temp.append(df.groupby(['Email'], sort=True).get_group(e).mode().head(1))

logger.debug(df)
# logger.debug(df_temp)
# logger.debug(unique_columns_data)
df.to_sql('clean_data', con, if_exists='replace')
# df_temp.to_sql('final_clean_data', con, if_exists='replace')


# pd.DataFrame(column_wise_error_inds).to_csv('data/error_inds.csv', index=False)
df.to_excel('data/data_new1.xlsx', index=False)
for key, val in column_wise_error_inds.items():
    logger.debug("{0} : {1}".format(key, val.values))
'''
#-------------------------------------------------------------------------------------------------

client = MongoClient()
ETL = client.ETL
jobs_left = ETL.jobs_left
jobs_completed = ETL.jobs_completed
list_of_processes = ['date','time','datetime','phone_number','gender','email','age','currency','other']
for x in jobs_left.find():
    job_id = x['_id']
    logger.debug(str(x['_id'])+"starts",extra={'job_id':job_id})
    meta_data = dict(x)
    # for getting values for above global variables
    for col_name, val in meta_data['columnMap'].items():
        columns[val[0]] = col_name
        if val[2] == 1:
            unique_columns[col_name] = val
            unique_columns_name.append(col_name)
    column_wise_error_inds = {}
    columns = {}
    dbname = meta_data["dbname"]
    tablename = meta_data["tablename"]
    conn = sqlite3.connect(dbname)
    df = pd.read_sql_query("SELECT * from '" + tablename + "'",conn)
    # for completing cleaning process for unique columns first
    for col_name, val in unique_columns.items():
        column_wise_error_inds[col_name], unique_columns_data[col_name] = operations(val[0])(col_name, val)
        completed_process.append(val[0])
    # # GLOBAL variable
    # For now we are ASSUMING that we have only one unique column that's why we are using unique_columns_name[0]
    # @unique_entity_values is in list format
    # only correct unique values are selectedn no nulls and errors
    # using this we will fill nan and errors of other columns data in cleaning process
    unique_entity_values = unique_columns_data[unique_columns_name[0]][column_wise_error_inds[unique_columns_name[0]] == 0].unique().tolist()

    # for completing cleaning process of other columns which are not unique and if we have a unique column preset
    # in data table then we will use unique_entity_values
    for col_name, val in meta_data['columnMap'].items():
        columns[val[0]] = col_name
    for col_name, val in meta_data['columnMap'].items():
        if col_name not in unique_columns:
            completed_process.append(val[0])
            column_wise_error_inds[col_name] = operations(val[0])(col_name, val)

    for process in list_of_processes:
        if process not in completed_process:
            logger.warning(process + " skiped",extra={'job_id':job_id})


    #for col_name, val in meta_data['columnMap'].items():
        #column_wise_error_inds[col_name] = operations(val[0])(col_name, val)
    #df.to_excel('data/data_new.xlsx', index=False)
    df_error=pd.DataFrame()
    droped_indices=[]
    for key, val in column_wise_error_inds.items():
        #print("{0} : {1}".format(key, val))
        if val is None:
            continue
        for i in range(len(val)):
            if val[i]==1 and i not in droped_indices:
                df_error = df_error.append(df.loc[i])
                df.drop([i],axis=0,inplace=True)
                droped_indices.append(i)
                print(droped_indices)
    logger.debug("Entering Data into FINAL Table",extra={'job_id':job_id})
    df.to_sql(meta_data['filename']+"final",conn)
    logger.debug("Entry Done in FINAL Table",extra={'job_id':job_id})
    logger.debug("Entering Data into ERROR Table",extra={'job_id':job_id})
    df_error.to_sql(meta_data['filename']+"error",conn)
    logger.debug("Entry Done in ERROR Table",extra={'job_id':job_id})
    conn.close()
    jobs_completed.insert_one(x)
    jobs_left.remove(x)
    logger.debug(str(x['_id'])+"job completed:)",extra={'job_id':job_id})
logger.debug("Cleaning Process Completed <3",extra={'job_id':job_id})

client = MongoClient()
logss = client.logs
logs = logss.logs
for l in logs.find({'job_id':job_id}):
    t_log = dict(l)
    print(t_log['timestamp'],"  ",t_log['message'])

