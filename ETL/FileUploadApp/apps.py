from django.apps import AppConfig


class FileuploadappConfig(AppConfig):
    name = 'FileUploadApp'
