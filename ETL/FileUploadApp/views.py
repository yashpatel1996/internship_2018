from django.shortcuts import render
from django.http import HttpResponse
from ETL.settings import AWS_ACCESS_KEY,AWS_SECRET_KEY,REGION_NAME,S3_BUCKET
import boto3
import time
def Upload(request):
    return render(request,"Upload.html")
def FileUpload(request):
    #filename = request.FILES["filename"]
    #print(filename)
    fileToUpload = request.FILES.get('filename')
    localtime = time.asctime( time.localtime(time.time()))
    cloudFilename = fileToUpload.name + localtime.replace(" ","")

    session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY,
                                    aws_secret_access_key=AWS_SECRET_KEY)
    s3 = session.resource('s3')
    s3.Bucket(S3_BUCKET).put_object(Key=cloudFilename, Body=fileToUpload)
    request.session["filename"] = cloudFilename
    return HttpResponse("Hello")
# Create your views here.
