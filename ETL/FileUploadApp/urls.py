from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$',views.Upload,name="Upload"),
    url(r'^uploadFunction/',views.FileUpload,name="File Upload")
]