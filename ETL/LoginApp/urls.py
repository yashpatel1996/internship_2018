from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$',views.LoginPage,name="Login"),
    url(r'^addDetails',views.addDetails,name="Add User Details")
]