from django.shortcuts import render
from django.http import HttpResponse


def start_home(request):
    return HttpResponse("<h2>Learn django with machine learning and data science</h2><br><a href='regression'>Regression</a>")
