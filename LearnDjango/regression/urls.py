from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='regression'),
    url(r'^linear-regression/submit', views.submit, name='info submit'),
    url(r'^linear-regression/$', views.linear_regression, name='linear regression'),

]