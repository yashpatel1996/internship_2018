import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


def calculate_point(text):
    stopwords1 = ['good','bad','nice','excellent','worst','superb','awesome','expensive','cheap','fine','outstanding','magnificent','fantastic','brilliant','robust']
    querywords = text.split()
    resultwords1  = [word for word in querywords if word.lower() in stopwords1]

    double_list = [querywords[i]+' '+querywords[i+1] for i in range(len(querywords)-1)]

    stopwords2 = ['not good','not bad']
    resultwords2  = [word for word in double_list if word.lower() in stopwords2]
    resultwords  = resultwords1 + resultwords2

    def countWords(A):
       dic={'fine':0,'superior':0,'outstanding':0,'magnificent':0,'fantastic':0,'brilliant':0,'robust':0,'bad': 0, 'worst': 0, 'cheap': 0, 'not bad': 0, 'superb': 0, 'excellent': 0, 'expensive': 0, 'awesome': 0, 'good': 0, 'nice': 0, 'not good': 0}
       cg=0
       cb=0
       for x in A:
                   #Python 2.7: if not dic.has_key(x):
              if x == "not good" and cg==0:
                dic[x]= A.count(x)
                dic["good"]-=A.count(x)
                cg=1
              elif x=="not bad" and cb==0:
                  dic[x]= A.count(x)
                  dic["bad"]-=A.count(x)
                  cb=1
              else:
                  dic[x]= A.count(x)
       return dic

    #print(countWords(resultwords))
    counters = countWords(resultwords)
    err_count=0
    positivity = (counters["good"] +0.9*counters["not bad"]+counters["nice"]+ 2*counters["excellent"] + counters["superb"]+ 1.65*counters["awesome"] + counters["superior"]+1.75*counters["outstanding"]+counters["magnificent"]+counters["fantastic"]+1.05*counters["brilliant"]+counters["robust"])
    negativity = (counters["bad"]+0.9*counters["not good"]+counters["worst"]+counters["cheap"])
    result_number = positivity-negativity

    if result_number > 0 :
        result = "Positive comment"
    elif result_number ==0:
        result = "moderate comment"
    else:
        result = "Negative comment"

    keys = []
    vals = []
    positivity_perc = positivity*100/(positivity+negativity)
    negativity_perc = negativity*100/(positivity+negativity)
    for key,value in counters.items():
        if value > 0:
            keys.append(key)
            vals.append(value)


    fig = plt.figure(figsize=(8, 4))
    ax = sns.barplot(keys, vals, alpha=0.8)
    plt.title("# per words")
    plt.ylabel('# of Occurrences', fontsize=12)
    plt.xlabel('Type ', fontsize=12)
    # adding the text labels
    rects = ax.patches
    labels = vals
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2, height + 5, label, ha='center', va='bottom')

    #fig.savefig('regression/templates/LinearRegression/foo.png')
    fig.savefig('regression/static/LinearRegression/foo.png')

    fig = plt.figure(figsize=(8, 4))
    ax = sns.barplot(['positivity','negativity'],[positivity_perc,negativity_perc],  alpha=0.8)
    plt.title("weightage plot")
    plt.ylabel('% of sentiment', fontsize=12)
    plt.xlabel('posivity vs negativity', fontsize=12)
    # adding the text labels
    rects = ax.patches
    labels = vals
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2, height + 5, label, ha='center', va='bottom')

    # fig.savefig('regression/templates/LinearRegression/foo.png')
    fig.savefig('regression/static/LinearRegression/fooperc.png')
    plt.close(fig)
    print(result_number)
    return {'result_number' : result_number, 'result' : result}