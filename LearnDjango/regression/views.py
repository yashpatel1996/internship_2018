from django.shortcuts import render
from django.http import HttpResponse
from . import ml_model

def index(request):
    return HttpResponse("<h2>Regression techniques for predictive modeling</h2><br><a href='linear-regression'>Linear Regression<a/>")


def linear_regression(request):
    return render(request, 'LinearRegression/linear-regression.html')


def submit(request):
    print("hello")
    #return HttpResponse("comment rating = " + str(val['result_number']) + "     comment evaluation = " + val['result'] )
    if 'info' in request.GET:
        info = request.GET['info']
        val = ml_model.calculate_point(info)
        #val['info'] = info
        return render(request, 'LinearRegression/linear-regression.html', val)
    else:
        return render(request, 'LinearRegression/linear-regression.html')
