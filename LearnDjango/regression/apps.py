from django.apps import AppConfig


class LinearregressionConfig(AppConfig):
    name = 'regression'
